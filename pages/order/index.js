//import {request} from '../../utils/request';
import {http} from '../../utils/base/http';
import {mustAuth} from '../../utils/util';

// pages/order/index.js
Page({

  /**
   * 页面的初始数据
   */
  data: {
    orders: []
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    mustAuth().then(() => {
      return http.get('auth/order')
    }).then((res) => {
      console.log(res)
      if (res.data.orders) {
        this.setData({
          orders: res.data.orders
        })
      }
    }).catch((reason) => {
      console.log(reason);
    })


    // mustAuth().then(() => {
    //   return http.get('auth/order')
    // }).then((res) => {
    //   if (res.data.orders) {
    //     this.setData({
    //       orders: res.data.orders
    //     })
    //   }
    // }).catch((reason) => {
    //   console.log(reason);
    // })
    // mustAuth().then(() => {
    //   return request('auth/order', {data: {}, method: 'get'})
    // }).then((res) => {
    //   if (res.data.orders) {
    //     this.setData({
    //       orders: res.data.orders
    //     })
    //   }
    // }).catch((reason) => {
    //   console.log(reason);
    // })
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function (options) {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {
    //console.log('hide');
  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {
    //console.log('unload');
  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})