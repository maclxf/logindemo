// pages/userLogin/index.js
//import {request} from '../../utils/base/request';
import {http} from '../../utils/base/http'
import {ensureSessionKey} from '../../utils/util';

Page({

  /**
   * 页面的初始数据
   */
  data: {
    user: {
      headUrl:'',
      nickName:'Hello!',
    },
    reference: '/pages/index/index'
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (e) {
    console.log(e)
    if (e.reference) {
      this.setData({
        reference: e.reference
      })
    }

    // 验证 sessionKey 是否过期，过期则刷新登录态
    // 最佳的我觉得应该还是在触发下面的函数时调用 wechatLogin
    // 但问题是我如何让在 ensureSessionKey 在catch时执行refreshLogin后还能再执行 wechatLogin
    ensureSessionKey()
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  },

  wechatLogin: function() {
    new Promise((reslove, reject) => {
      wx.getUserProfile({
        desc: '用于完善会员资料', // 声明获取用户个人信息后的用途，后续会展示在弹窗中，请谨慎填写
        success: (result) => {
          reslove(result)
        },
        fail: (err) => reject('no way'),
        complete: () => {}
      })
    }).then((result) => {
      return http.post('/auth/encrypetdUserInfo', {
        iv: result.iv,
        encryptedData: result.encryptedData,
        signature: result.signature
      })
      // request('auth/encrypetdUserInfo', {method: 'post', data: {
      //   iv: result.iv,
      //   encryptedData: result.encryptedData,
      //   signature: result.signature
      // }}).then((result) => {
      //   let data = result.data;
      //   this.setData({
      //     user:data.userInfo
      //   })
      //   return new Promise((resolve, reject) => {
      //     wx.setStorage({
      //       key: 'userInfo',
      //       data: data.userInfo,
      //       success: (data) => {
      //         resolve(data)
      //       },
      //       fail: (err) => {
      //         reject('set user info error');
      //       },
      //     });
      //   });
      // })
    }).then((result) => {
      let data = result.data;
      this.setData({
        user:data.userInfo
      })
      return new Promise((resolve, reject) => {
        wx.setStorage({
          key: 'userInfo',
          data: data.userInfo,
          success: (data) => {
            // getLastPageUrl() 这里也可以用这个函数来获取上一个地址，相比之下传进来就是需要一直带上参数走
            wx.redirectTo({
              url: this.data.reference,
            });
            resolve(data)
          },
          fail: (err) => {
            reject('set user info error');
          },
        });
      });
    })
  },
})