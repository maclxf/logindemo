import {http} from '../../utils/base/http';
import {ensureSessionKey} from '../../utils/util';
//import {request} from '../../utils/request';

// pages/login/index.js
Page({

  /**
   * 页面的初始数据
   */
  data: {
    reference: ''
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (e) {
    console.log(e)
    if (e.reference) {
      this.setData({
        reference: e.reference
      })
    }
    // 验证 sessionKey 是否过期，过期则刷新登录态
    // 最佳的我觉得应该还是在触发下面的函数时调用 getPhone
    // 但问题是我如何让在 ensureSessionKey 在catch时执行refreshLogin后还能再执行 getPhone
    ensureSessionKey()
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  },


  getPhone: function(e) {
    let detail = e.detail;

    if (!detail.iv || !detail.encryptedData) {
      console.log('sorry no phone detail')
      return
    }

    // return request('auth/encrypetdPhone', {method: 'post', data: {
    //   iv: detail.iv,
    //   encryptedData: detail.encryptedData
    // }})
    http.post('auth/encrypetdPhone', {
      iv: detail.iv,
      encryptedData: detail.encryptedData
    }).then((result) => {
      let data = result.data;
      return new Promise((resolve, reject) => {
        wx.setStorage({
          key: 'userInfo',
          data: data.userInfo,
          success: (data) => {
            resolve(data)
          },
          fail: (err) => {
            reject('set user info error');
          },
        });
      });
    }).then((data) => {
      wx.redirectTo({
        url: '/pages/userLogin/index?reference=' + this.data.reference,
      });
    }).catch((reason) => {
      console.log(reason)
      // if (reason == 403) {
      //   refreshLogin()
      // }
    })
  }
})