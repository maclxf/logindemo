import { AUTHENTICATION, TOKEN } from "../../config/config.js";
import {slientLogin, refreshLogin} from '../util'
import {flyer} from '../base/flyer'
const http = flyer.create();

http.interceptors.request.use((request) => {
  //console.log(request)
  let url = request.url
  //console.log(url);
  //console.log(`${AUTHENTICATION}/`);
  console.log(url.includes(`${AUTHENTICATION}/`));
  if (url.includes(`${AUTHENTICATION}/`)) {
    let token = wx.getStorageSync(TOKEN);
    if (token) {
      request.headers[AUTHENTICATION] = "Bearer " + token;
      return request
    } else {
      //console.log(request)
      http.lock()
      return refreshLogin().then(() => {
        let token = wx.getStorageSync(TOKEN);
        request.headers[AUTHENTICATION] = "Bearer " + token;

        return request
      }).finally(()=>{
        http.unlock()
      })
    }
  } else {
    return request
  }
})

http.interceptors.response.use(function(response) {
  //console.log(response)

  let result = response.data ? response.data : "";
  if (!result) {
    return response
  }
  // console.log(res)
  //console.log(result)
  if (result.code == 20000) {
    return result
  }  else if (result.code == 40004) {
    //console.log('not refresh')
    this.lock()
    return slientLogin()
            .finally(() => this.unlock()) //解锁
            .then(() => {
              //log(`重新请求：path:${response.request.url}，baseURL:${response.request.baseURL}`)
              return http.request(response.request);
            })
  } else {
    return result
  }

  },
  function(err) {
    console.log(err)
  }
)

export {http};