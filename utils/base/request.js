// 2021-05-26 弃用暂时保留代码
import { HTTP_REQUEST_URL, AUTHENTICATION, TOKEN } from "../../config/config.js";
import {slientLogin, refreshLogin} from '../util'
export const request = function (path, params) {
  //console.log(path);
  //console.log(params);
  let url = HTTP_REQUEST_URL + path;
  //console.log(url);

  // let header = {}

  let header = {}
  if (url.includes(`/${AUTHENTICATION}`)) {
    let token = wx.getStorageSync(TOKEN);
    if (token) {
      header[AUTHENTICATION] = "Bearer " + token;
    } else {
      //console.log('refresh')
      //TODO::这里有问题当一个页面发起N个请求时都会到这里那么这里会发起N次refresh
      refreshLogin().then(() => {
        let token = wx.getStorageSync(TOKEN);
        header[AUTHENTICATION] = "Bearer " + token;
      })
    }
  }

  return new Promise((resolve, reject) => {
    wx.request({
      url,
      header,
      method: params.method || 'get',
      data: params.data || {},
      success: (res) => {
        let result = res.data ? res.data : "";
        if (!result) {
          reject(res.errMsg);
        }
        // console.log(res)
        // console.log(result)
        if (result.code == 20000) {
          resolve(result);
        }  else if (result.code == 40004) {
          console.log('not refresh')
          slientLogin().then(() => {
            resolve(request(path, params))
          });
        } else {
          reject(result)
        }
      },
      fail: (error) => reject(error),
    });
  });
};
