import { HTTP_REQUEST_URL } from "../../config/config.js";
var Fly = require('../vendor/wx.umd.min')

const flyer = {

  create() {
    let fly = new Fly();
    fly.config.baseURL = HTTP_REQUEST_URL

    return fly
  }
}

export {flyer}