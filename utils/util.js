import { TOKEN } from '../config/config';
//import {request} from './request';
//import {http} from './base/http';
import {wxp} from './wxp';
import {flyer} from './base/flyer'

// console.log(http == flyer.create())
// console.log(flyer.create())
// var Fly = require("wx.umd.min") //wx.js为您下载的源码文件
// const tokenFly = new Fly();
// tokenFly.config = http.config

const formatTime = date => {
  const year = date.getFullYear()
  const month = date.getMonth() + 1
  const day = date.getDate()
  const hour = date.getHours()
  const minute = date.getMinutes()
  const second = date.getSeconds()

  return `${[year, month, day].map(formatNumber).join('/')} ${[hour, minute, second].map(formatNumber).join(':')}`
}

const formatNumber = n => {
  n = n.toString()
  return n[1] ? n : `0${n}`
}

const slientLogin = () => {
  return  wxp.login().then((result) => {
    let code = result.code
    //return request('wechatLogin', {data: {code}, method: 'post'})
    // 必须另起一个fly因为开始一个被lock
    return flyer.create().post('wechatLogin', {code})
  }).then((result) => {
    // 因为这里result是上面的flyer返回没经过拦截器处理所以要data.data
    let data = result.data.data;
    return wxp.setStorage({
      key: TOKEN,
      data: data.authToken,
    })
  }).then((data) => {
    return wxp.setStorage({
      key: 'userInfo',
      data: data.userInfo,
    })
  }).catch((error) => {
    console.log(error)
  })
  // return new Promise((resolve, reject) => {
  //   wx.login({
  //     success: (result) => {
  //       console.log(result);
  //       let code = result.code;
  //       if (code) {
  //         resolve(code)
  //       } else {
  //         reject('no code 1')
  //       }
  //     },
  //     fail: (err) => {
  //       reject('no code 2');
  //     },
  //   });
  // }).then((code) => {
  //   //return request('wechatLogin', {data: {code}, method: 'post'})
  //   // 必须另起一个fly因为开始一个被lock
  //   return flyer.create().post('wechatLogin', {code})
  // }).then((result) => {
  //   console.log(result)
  //   let data = result.data.data;
  //   return new Promise((resolve, reject) => {
  //     wx.setStorage({
  //       key: TOKEN,
  //       data: data.authToken,
  //       success: (result) => {
  //         resolve(data)
  //       },
  //       fail: (err) => {
  //         reject('set auth token error');
  //       },
  //     });
  //   });
  // }).then((data) => {
  //   return new Promise((resolve, reject) => {
  //     wx.setStorage({
  //       key: 'userInfo',
  //       data: data.userInfo,
  //       success: (data) => {
  //         resolve(data)
  //       },
  //       fail: (err) => {
  //         reject('set user info error');
  //       },
  //     });
  //   });
  // }).catch((error) => {
  //   console.log(error)
  // })
}


const login = () => {
  wxp.getStorage({key: TOKEN}).then(() => {
    //console.log(a)
    return wxp.checkSession()
  }).then(() => {
    //console.log(b)
  }).catch((reason) => {
    console.log(reason)
    slientLogin()
  })
  // new Promise((resolve, reject) => {
  //   wx.getStorage({
  //     key: 'token',
  //     success: (result) => {
  //       console.log(result);
  //       resolve(true);
  //     },
  //     fail: (err) => {
  //       console.log(err)
  //       reject(false)
  //     },
  //   });
  // }).then((hasToken) => {
  //   return new Promise((resolve, reject) => {
  //     wx.checkSession({
  //       success: (result) => {
  //         console.log(result);
  //         resolve(true);
  //       },
  //       fail: (err) => {
  //         console.log(err)
  //         reject(false)
  //       },
  //       complete: () => {}
  //     });
  //   })
  // }).then((checkSession) => {
  //   console.log('ok')
  // }).catch((reason) => {
  //   console.log(reason)
  //   slientLogin()
  // })
}

const refreshLogin = () => {
  return wxp.clearStorage().then(() => {
    //console.log(clearResult)
    return slientLogin()
  }).catch((reason) => {
    console.log(reason)
  })

  // return new Promise((resolve, reject) => {
  //   wx.clearStorage({
  //     success: (result) => {
  //       console.log(result);
  //       resolve(true);
  //     },
  //     fail: (err) => {
  //       console.log(err)
  //       reject(false)
  //     },
  //   })
  // }).then((clearFinish) => {
  //   return slientLogin()
  // }).catch((reason) => {
  //   console.log(reason)
  // })
}

const mustAuth = () => {
  if (getCurrentAuthStep() >= 2) {
    return Promise.resolve();
  }

  let url = getCurrentPageUrl()

  // wx.navigateTo({}) ，保留当前页面，跳转到应用内的某个页面，
  // wx.redirectTo关闭当前页面,跳转到另外一个页面，他的效果比较清奇当前页面关闭后，会把之前在战中的页面弹出来，效果就是出现关闭新页面重现老页面再到跳转的页面
  wx.navigateTo({
    url: '/pages/login/index?reference=' + url
  })
  // 执行reject操作
  return Promise.reject('need auth step more than 2');
}

const getCurrentAuthStep = () => {
  // 可能还有些逻辑比如千幻账号永远返回1

  let userInfo = getUser();
  if (userInfo.busiIdentity !== 'MEMBER') return 1;

  if (!userInfo.headUrl) return 2;

  return 3;
}


const getUser = () => {
  return wx.getStorageSync('userInfo');
}

const ensureSessionKey = () => {
  return wxp.checkSession().catch((err) => {
    //console.log('ensuresessionkey', err)
    refreshLogin()
  })

  // new Promise((resolve, reject) => {
  //   wx.checkSession({
  //     success () {
  //       resolve()
  //     },
  //     fail (err) {
  //       console.log('ensuresessionkey' + err)
  //       // session_key 已经失效，需要重新执行登录流程
  //       refreshLogin()
  //       reject(403) //重新登录
  //     }
  //   })
  // })
}


function getCurrentPageUrl() {
  return getPageUrl()
}

function getLastPageUrl() {
  return getPageUrl(-1)
}

function getPageUrl($step = 0) {
  const pages = getCurrentPages()
  const currentPage = pages[pages.length - 1 + $step]
  const url = `/${currentPage.route}`
  return url
}

export {
  formatTime,
  slientLogin,
  login,
  refreshLogin,
  mustAuth,
  getCurrentAuthStep,
  ensureSessionKey,
  getCurrentPageUrl,
  getLastPageUrl,
  getPageUrl
}
