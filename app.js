// app.js
import {login} from './utils/util';



App({
  onLaunch() {
    login();
  },
  globalData: {
    userInfo: null
  }
})
