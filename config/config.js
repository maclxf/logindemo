const HTTP_REQUEST_URL = 'http://mp-tp-login.top/v1/';
const AUTHENTICATION = 'auth';
const TOKEN = 'token';

export {
  HTTP_REQUEST_URL,
  AUTHENTICATION,
  TOKEN
}